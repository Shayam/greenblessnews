
	<div class="row">
		
		
<div class="col-lg-12">
	
	<div class="card text-white bg-secondary my-4 ">
		<div class="card-body">
		
			<h3>
				<img src="${contextRoot}/resources/images/HeadingLocal.jpg" alt=""
					style="height: 30px; width: 5px" />&nbsp<b id="TitleofNews" style="color: gray;">  </b>
			</h3>
			
			<script type="text/javascript">
			var catname = window.categoryName;
			if(typeof catname != 'undefined'){
			document.getElementById("TitleofNews").innerHTML = catname;
			}else{
				document.getElementById("TitleofNews").innerHTML = "Home";
			}
			
			</script>
			<hr />
		</div>
	</div>
	<div class="row">
	<div class="col-md-1"></div>
		<div class="col-md-10">
		<c:forEach items="${newsFeedDAO}" var="newsfeed">
			<!--adding the main news -->
			<c:if test="${newsfeed.priority==1}">
				<div class="row my-4">
					<div class="col-lg-7">
						<img class="img-fluid rounded"
							src="<%=request.getContextPath()%>/resources/images/${newsfeed.image}.jpg"
							alt="No Image Found" style="width: 100%; height: 300px" />
					</div>
					<!-- /.col-lg-8 -->
					<div class="col-lg-4">
						<h3>${newsfeed.heading}</h3>
						<div class="block-ellipsis">
							<p>${newsfeed.description}</p>
						</div>
						
						<a class="btn btn-primary btn-lg" href="${contextRoot}/show/${newsfeed.idNewsFeed}/newsfeed">Read More..</a>
					</div>
					<!-- /.col-md-4 -->
				</div>
			</c:if>
			<br/>
		</c:forEach>
		<br />
		<br />
		<c:forEach items="${newsFeedDAO}" var="newsfeed">
			<!--Adding the second level news-->
			<c:if test="${newsfeed.priority==2}">
				<div class="col-md-4 mb-4">
					<div class="card h-100">
						<div class="card-body">
							<h4 class="card-title">${newsfeed.heading}</h4>
							<img class="img-fluid rounded"
								src="${contextRoot}/resources/images/${newsfeed.image}.jpg"
								alt="No Image Found" style="width: 100%; height: 150px"></img>
							<div class="block-ellipsis-group2">
								<p class="card-text">${newsfeed.description}</p>
							</div>
						</div>
						<div class="card-footer">
							<a href="${contextRoot}/show/${newsfeed.idNewsFeed}/newsfeed" class="btn btn-primary">More Info</a>
						</div>
					</div>
				</div>
			</c:if>
		</c:forEach>
		</div>
		<div class="col-md-1"></div>
		
	</div>
	<hr />
		</div>
</div>


	<!-- 	<div class="row"> -->

	<!-- Heading Row -->

	<!-- /.row -->

	<!--Testing Datatable -->

	<!-- 		<div class="row my-4"> -->
	<!-- 			<div class="col-xs-12"> -->

	<!-- 				<table id="newsTable" class="table table-striped table-borderd"> -->
	<!-- 					<thead> -->
	<!-- 						<tr> -->
	<!-- 							<th></th> -->
	<!-- 							<th>Heading</th> -->
	<!-- 							<th>Description</th> -->
	<!-- 							<th></th> -->
	<!-- 						</tr> -->
	<!-- 					</thead> -->

	<!-- 					<tfoot> -->
	<!-- 						<tr> -->
	<!-- 							<th></th> -->
	<!-- 							<th>Heading</th> -->
	<!-- 							<th>Description</th> -->
	<!-- 							<th></th> -->
	<!-- 						</tr> -->
	<!-- 					</tfoot> -->
	<!-- 				</table> -->
	<!-- 			</div> -->
	<!-- 		</div> -->
