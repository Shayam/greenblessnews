<spring:url var="css" value="/resources/css" />
<style>
<%@includefile="/assets/css/myapp.css"%>
</style>

<div class="row">
	<div class="col-lg-12">

		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<img
						src="<%=request.getContextPath()%>/resources/images/male-02.jpg"
						alt="Beauty Of Sri Lanka" style="width: 100%; height: 440px">
				</div>

				<div class="item">
					<img
						src="<%=request.getContextPath()%>/resources/images/slide-02.jpg"
						alt="Beauty Of Sri Lanka" style="width: 100%; height: 440px">
				</div>

				<div class="item">
					<img
						src="<%=request.getContextPath()%>/resources/images/Fisher.jpg"
						alt="Beauty Of Sri Lanka" style="width: 100%; height: 440px">
				</div>
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span> <span
				class="sr-only">Previous</span>
			</a> <a class="right carousel-control" href="#myCarousel"
				data-slide="next"> <span
				class="glyphicon glyphicon-chevron-right"></span> <span
				class="sr-only">Next</span>
			</a>
		</div>

	</div>
	<br /> <br />
	<hr />



	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<hr>
			<div class="card text-white bg-secondary my-4" style="height: 30px">
				<div class="card-body">

					<h4>
						<img src="${contextRoot}/resources/images/HeadingSport.jpg" alt=""
							style="height: 30px; width: 5px" />&nbsp<b id="TitleofNews"
							style="color: gray;">MOST POPULAR</b>
					</h4>

					</script>
					<hr />
				</div>
			</div>



			<c:set var="count" value="0" scope="page" />

			<c:forEach items="${newsFeedDAO}" var="newsfeed">
				<!--Adding the second level news-->

				<c:if test="${count!=4}">
					<c:if test="${newsfeed.priority==1}">

						<c:set var="count" value="${count + 1}" scope="page" />

						<div class="col-md-6 mb-6">
							<div class="card h-100">

								<div class="mycontainer">

									<div class="card-body">
										<h2 class="card-title"></h2>
										<img
											src="${contextRoot}/resources/images/${newsfeed.image}.jpg"
											alt="No Image Found" style="width: 100%; height: 200px"></img>

										<!-- 							<div class="block-ellipsis-group2"> -->
										<%-- 								<p class="card-text">${newsfeed.description}</p> --%>
										<!-- 							</div> -->
									</div>


									<a href="${contextRoot}/show/${newsfeed.idNewsFeed}/newsfeed">
										<div class="myoverlay">
											<div class="mytext">${newsfeed.heading}</div>
										</div>
									</a>
								</div>
							</div>
						</div>

					</c:if>
				</c:if>

			</c:forEach>
		</div>
		<div class="col-md-1"></div>
	</div>




	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<hr>
			<div class="card text-white bg-secondary my-4" style="height: 30px">
				<div class="card-body">

					<h4>
						<img src="${contextRoot}/resources/images/HeadingLocal.jpg" alt=""
							style="height: 30px; width: 5px" />&nbsp<b id="TitleofNews"
							style="color: gray;">RECOMMENDED</b>
					</h4>

					</script>
					<hr />
				</div>
			</div>

			<c:set var="count" value="0" scope="page" />

			<c:forEach items="${newsFeedDAO}" var="newsfeed">
				<!--Adding the second level news-->

				<c:if test="${count!=8}">
					<c:if test="${newsfeed.priority==2}">

						<c:set var="count" value="${count + 1}" scope="page" />

						<div class="col-md-3 mb-3">
							<div class="card h-100">

								<div class="card-body">
									<div
										style="width: 100%; height: 15px; background-color: blue; opacity: 0.8; filter: alpha(opacity = 80);">
										<font color="white"><b><h5 class="card-title">${newsfeed.heading}</h5></b></font>
									</div>
									<div class="myzoom">
										<a href="${contextRoot}/show/${newsfeed.idNewsFeed}/newsfeed">
											<img class="img-fluid rounded"
											src="${contextRoot}/resources/images/${newsfeed.image}.jpg"
											alt="No Image Found" style="width: 100%; height: 100px"></img>
										</a>
									</div>
									<div class="block-ellipsis-group2">
										<p class="card-text">${newsfeed.description}</p>
									</div>
								</div>



							</div>
						</div>

					</c:if>
				</c:if>

			</c:forEach>
		</div>
		<div class="col-md-1"></div>
	</div>
</div>
