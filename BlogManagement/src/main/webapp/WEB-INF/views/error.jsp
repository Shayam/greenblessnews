<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="images" value="/resources/images" />
<c:set var="contextRoot" value="${pageContext.request.contextPath}" />


<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">



<title>News-GreenBless - ${title}</title>


<!-- Bootstrap Core CSS -->
<link href="${css}/bootstrap.min.css" rel="stylesheet" type="text/css">



<!-- Custom CSS -->
<link href="${css}/sb-admin-2.css" rel="stylesheet" type="text/css">



<link href="${css}/error.css" rel="stylesheet" type="text/css" />




<!-- Custom Fonts -->
<link
	href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<!-- 		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> -->

		<nav class="navbar navbar-inverse navbar-light bg-light"
			role="navigation">
			<div class="container-fluid">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" href="${contextRoot}/login">GreenBless
							News</a>
					</div>
				</div>
			</div>

		</nav>



		<div class="content">

			<div class="container">
				<section id="not-found">
					<div id="title">GreenBless News &bull; ${errorTitle} Error Page</div>
					<div class="circles">
						<p>
							${errorTitle}<br> <small>${errorDescription}</small>
						</p>
						<span class="circle big"></span> <span class="circle med"></span>
						<span class="circle small"></span>
					</div>
				</section>
			</div>
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="${js}/jquery.min.js" type="text/javascript"></script>


	<!-- jQueryValidator -->
	<script src="${js}/jquery.validate.js" type="text/javascript"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="${js}/bootstrap.min.js" type="text/javascript"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="${js}/metisMenu.min.js" type="text/javascript"></script>
	<script src="${js}/navbar.js" type="text/javascript"></script>
	<script src="${js}/myapp.js" type="text/javascript"></script>
	<!-- Custom Theme JavaScript -->
	<script src="${js}/sb-admin-2.js" type="text/javascript"></script>

</body>

</html>