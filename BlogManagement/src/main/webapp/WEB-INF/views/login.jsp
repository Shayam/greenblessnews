<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="images" value="/resources/images" />
<c:set var="contextRoot" value="${pageContext.request.contextPath}" />


<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">



<title>News-GreenBless - ${title}</title>
<script>
	window.menu = '${title}';
	window.contextRoot = '${contextRoot}';
</script>

<!-- Bootstrap Core CSS -->
<link href="${css}/bootstrap.min.css" rel="stylesheet" type="text/css">



<!-- Custom CSS -->
<link href="${css}/sb-admin-2.css" rel="stylesheet" type="text/css">


<!-- Adding my CSS -->
<link href="${css}/myapp.css" rel="stylesheet" type="text/css" />




<!-- Custom Fonts -->
<link
	href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<!-- 		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> -->

		<nav class="navbar navbar-inverse navbar-light bg-light"
			role="navigation">
			<div class="container-fluid">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" href="${contextRoot}/home">GreenBless
							News</a>
					</div>
				</div>
			</div>

		</nav>



		<div class="content">

			<div class="container">
				<!--this will be displayed if the credentials are wrong-->
				<c:if test="${not empty message}">
					<div class="row">
						<div class="col-md-offset-3 col-md-6">
							<div class="alert alert-danger">${message}</div>
						</div>
					</div>
				</c:if>


				<div class="row">
					<div class="col-md-offset-3 col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h4>Login</h4>
							</div>

							<div class="panel-body">
								<form action="${contextRoot}/login" method="POST"
									class="form-horizontal" id="loginForm">
									<div class="form-group">
										<font color="black"><label for="username"
											class="col-md-4 control-label">Email:</label></font>
										<div class="col-md-8">
											<input type="text" name="username" id="username"
												class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<font color="black"><label for="password"
											class="col-md-4 control-label">Password:</label></font>
										<div class="col-md-8">
											<input type="password" name="password" id="password"
												class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-offset-4 col-md-8">
											<input type="submit" value="Login" class="btn btn-primary" />
											<input type="hidden" name="${_csrf.parameterName}"
												value="${_csrf.token}" />
										</div>
									</div>
								</form>

							</div>
							<div class="panel-footer">
								<div class="text-right">
									<font color="black">New User - <a
										href="${contextRoot}/register">Register Here</a></font>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="${js}/jquery.min.js" type="text/javascript"></script>


	<!-- jQueryValidator -->
	<script src="${js}/jquery.validate.js" type="text/javascript"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="${js}/bootstrap.min.js" type="text/javascript"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="${js}/metisMenu.min.js" type="text/javascript"></script>
	<script src="${js}/navbar.js" type="text/javascript"></script>
	<script src="${js}/myapp.js" type="text/javascript"></script>
	<!-- Custom Theme JavaScript -->
	<script src="${js}/sb-admin-2.js" type="text/javascript"></script>

</body>

</html>