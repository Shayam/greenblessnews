<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="images" value="/resources/images" />
<c:set var="contextRoot" value="${pageContext.request.contextPath}" />


<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<meta name="_csrf" content="${_csrf.token}">
<meta name="_csrf_header" content="${_csrf.headerName}">

<title>News-GreenBless - ${title}</title>
<script>
	window.menu = '${title}';
	window.contextRoot = '${contextRoot}';
</script>

<!-- Bootstrap Core CSS -->
<link href="${css}/bootstrap.min.css" rel="stylesheet" type="text/css">

<!-- MetisMenu CSS -->
<link href="${css}/metisMenu.min.css" rel="stylesheet" type="text/css">

<!-- Custom CSS -->
<link href="${css}/sb-admin-2.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="${css}/style5.css" type="text/css">


<!-- Adding my CSS -->
<link href="${css}/myapp.css" rel="stylesheet" type="text/css" />

<!-- BootsTrap DataTables -->
<link href="${css}/dataTables.bootstrap.css" rel="stylesheet"
	type="text/css">

<!-- BootsTrap DataTables4 -->
<link href="${css}/dataTables.bootstrap4.css" rel="stylesheet"
	type="text/css">

<!-- Morris Charts CSS -->
<link href="${css}/morris.css" rel="stylesheet" type="text/css">



<!-- Custom Fonts -->
<link
	href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div class="wrapper">
		<nav id="sidebar">
			<div class="sidebar-header">
				<img src="<%=request.getContextPath()%>/resources/images/Logo.gif"
					alt="" width="100%" height="100%">
			</div>

			<ul class="list-unstyled components">
				<p>Category</p>
				<li><a href="${contextRoot}/home"> <i class="fa fa-home"
						style="font-size: 24px;"></i> Home
				</a></li>



				<c:forEach items="${categoryDAO}" var="category">
					<li><a href="${contextRoot}/show/${category.id}/newsondiv">
							<i><img
								src="<%=request.getContextPath()%>/resources/images/${category.imageURL}"
								alt="" width="24px" height="24px"> </i> ${category.name}
					</a></li>
				</c:forEach>
			</ul>


		</nav>
		<script>
			window.categoryId = '';
		</script>
		<!-- Page Content Holder -->
		<div id="content">

			<nav class="navbar navbar-expand-lg navbar-light bg-light">

				<div class="col-lg-12">
					<div class="col-lg-1">
						<button type="button" id="sidebarCollapse" class="navbar-btn">
							<span></span> <span></span> <span></span>
						</button>
					</div>
					<div class="col-lg-6">
						<h2>
							Welcome to <span style="color: green;">Green</span>Bless News.
						</h2>
					</div>
					<div class="col-lg-5">
					<div class="nav navbar-nav navbar-right">
							<button class="btn btn-info" type="button" id="register">
								<a href="${contextRoot}/signup">Signup</a>
							</button>
						
							<button class="btn btn-success" type="button" id="login">
								<a href="${contextRoot}/login">Login</a>
							</button>
							<button class="btn btn-default dropdown-toggle" type="button"
								data-toggle="dropdown" id="dropdownMenu1">
								Actions <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li class="dropdown-header">Account</li>
								<li><a href="#">Settings</a></li>
								<li><a href="#">Menu 2</a></li>
								<li><a href="#">Menu3</a></li>
								<li class="divider"></li>
								<li class="dropdown-header">Other</li>
								<li><a href="#">About Us</a></li>
								<li><a href="${contextRoot}/logout">Logout</a></li>
							</ul>
						
						</div>



						<!-- 						<ul class="nav navbar-nav navbar-right"> -->
						<%-- 							<li id="register"><a href="${contextRoot}/register">SignUp</a></li> --%>
						<%-- 							<li id="login"><a href="${contextRoot}/login">Login</a></li> --%>

						<!-- 							<li class="dropdown"><a href="javascript:void(0)" -->
						<!-- 								class="btn btn-default dropdown-toggle" id="dropdownMenu1" -->
						<!-- 								data-toggle="dropdown"> Full Name <span class="caret"></span> -->
						<!-- 							</a> -->
						<!-- 								<ul class="dropdown-menu"> -->
						<%-- 									<li><a href="${contextRoot}/logout">Logout</a></li> --%>
						<!-- 								</ul></li> -->
						<!-- 						</ul> -->
					</div>
				</div>





			</nav>
			<!--home content -->
			<c:if test="${userClickHome==true}">
				<script>
					window.categoryId = '';
				</script>
				<%@include file="home2.jsp"%>
			</c:if>

			<!--Categories -->
			<c:if test="${userClickCategory==true}">
				<script>
					window.categoryId = '${category.id}';
				</script>
				<%@include file="home.jsp"%>
			</c:if>

			<c:if test="${userClickAddtoDiv==true}">
				<script>
					window.categoryName = '${category.name}';
				</script>
				<%@include file="home.jsp"%>
			</c:if>

			<c:if test="${userClickSingleNews==true}">
				<script>
					window.categoryName = '${category.name}';
				</script>
				<%@include file="singleNews.jsp"%>
			</c:if>

		</div>
	</div>


	<div
		class="a2a_kit a2a_kit_size_32 a2a_floating_style a2a_vertical_style"
		style="right: 0px; top: 150px;">
		<a class="a2a_button_facebook"></a> <a class="a2a_button_twitter"></a>
		<a class="a2a_button_google_plus"></a> <a class="a2a_button_pinterest"></a>
		<a class="a2a_dd" href="https://www.addtoany.com/share"></a>
	</div>

	<script async src="https://static.addtoany.com/menu/page.js"></script>


	<!-- jQuery CDN - Slim version (=without AJAX) -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<!-- Popper.JS -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
		integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
		crossorigin="anonymous"></script>
	<!-- Bootstrap JS -->
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
		integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
		crossorigin="anonymous"></script>

	<script type="text/javascript">
		function Login() {
			window.open("${contextRoot}/login", "_self");
		}

		$(document).ready(function() {
			$('#sidebarCollapse').on('click', function() {
				$('#sidebar').toggleClass('active');
				$(this).toggleClass('active');
			});
		});
	</script>

	<!-- jQuery -->
	<script src="${js}/jquery.min.js" type="text/javascript"></script>

	<!-- jQueryValidator -->
	<script src="${js}/jquery.validate.js" type="text/javascript"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="${js}/bootstrap.min.js" type="text/javascript"></script>
	<!--adding Jquery Tables -->
	<script src="${js}/jquery.dataTables.js" type="text/javascript"></script>

	<!--adding bootstrapDataTables Tables -->
	<script src="${js}/dataTables.bootstrap.js" type="text/javascript"></script>

	<!--adding bootstrapDataTables4 Tables -->
	<script src="${js}/dataTables.bootstrap4.js" type="text/javascript"></script>

	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- Metis Menu Plugin JavaScript -->
	<script src="${js}/metisMenu.min.js" type="text/javascript"></script>

	<!-- Morris Charts JavaScript -->
	<script src="${js}/raphael.min.js" type="text/javascript"></script>
	<script src="${js}/morris.min.js" type="text/javascript"></script>
	<script src="${js}/morris-data.js" type="text/javascript"></script>
	<script src="${js}/navbar.js" type="text/javascript"></script>
	<script src="${js}/myapp.js" type="text/javascript"></script>
	<!-- Custom Theme JavaScript -->
	<script src="${js}/sb-admin-2.js" type="text/javascript"></script>

</body>

</html>