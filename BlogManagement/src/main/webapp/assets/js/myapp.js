$(function() {

	//to tackle the csrf token 
	var token = $('meta[name="_csrf"]').attr('content');
	var header = $('meta[name="_csrf_header"]').attr('content');
	
	if(token.length > 0 && header.length > 0){
		//set the token header for AJAX request
		$(document).ajaxSend(function(e,xhr,options){
			xhr.setRequestHeader(header,token);
		});
	}
	
	var $table = $('#newsTable');

//Execute the below code only where we have this table

if($table.length){
	
	var jsonUrl ='';
	if(window.categoryId==''){
		jsonUrl = window.contextRoot+'/json/data/all/newsfeed';
	}else{
		jsonUrl = window.contextRoot+'/json/data/category/'+window.categoryId+'/newsfeed';
	}
	
	
	
	$table.DataTable({
				lengthMenu: [[10,20,50,-1],['10 Records','20 Records','50 Records','All Records']],
				pageLength:5,
				ajax:{
					url:jsonUrl,
					dataSrc:''
				},
				columns:[
					{
						data:'image',
						mRender:function(data,type,row){
							return '<img src="'+window.contextRoot+'/resources/images/'+data+'.jpg" class="dataTableImg"/>';
						}
							
					},
					{
						data:'heading'
					},
					{
						data:'description'
					},
					{
						data:'idNewsFeed',
						bSortable:false,
						mRender:function(data,type,row){
							var str = '';
							str+='<a href="'+window.contextRoot+'/show/'+data+'/newsfeed" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"</span></a>';
							return str;
						}
							
					}
					
				]
			});
}
//...................................................
//Validation For Login Form
var $loginForm =$('#loginForm');
if($loginForm.length){
	$loginForm.validate({
		rules:{
			username:{
				required:true,
				email:true
			},
			password:{
				required:true
			}
		},
		message:{
			username:{
				required:'Please enter the User Name !',
				email:'Please enter valid email address!'
			},
			password:{
				required:'Please enter the password!'
			}
		},
		errorElement:'em',
		errorPlacement:function(error,element){
			error.addClass('help-block');
			error.insertAfter(element);
		}
		
	});
}









});

