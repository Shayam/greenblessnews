package net.kr.BlogManagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.kr.BlogManagementBackend.dao.CategoryDAO;
import net.kr.BlogManagementBackend.dao.NewsFeedDAO;
import net.kr.BlogManagementBackend.dto.Category;
import net.kr.BlogManagementBackend.dto.NewsFeed;

@Controller
public class PageController {
	
	@Autowired
	private CategoryDAO categoryDAO;
	@Autowired
	private NewsFeedDAO newsFeedDAO;
	
	@RequestMapping(value = {"/", "/home", "/index"})
	
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title","Home");
		mv.addObject("categoryDAO",categoryDAO.list());
		mv.addObject("newsFeedDAO",newsFeedDAO.listActiveNews());
		mv.addObject("userClickHome",true);
		return mv; 
	}
	
@RequestMapping(value = {"/inventoryhome"})
	
	public ModelAndView inventoryhome() {
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title","Inventory Home");
		mv.addObject("userClickInventoryHome",true);
		return mv; 
	}
	
//Mthods to load all products based on category. 

@RequestMapping(value = "/show/category/{id}/products")
public ModelAndView showCategoryProducts(@PathVariable("id") int id) {
	ModelAndView mv = new ModelAndView("page");
	Category category = null;
	category = categoryDAO.get(id);
	mv.addObject("title",category.getName());
	mv.addObject("categoryDAO",categoryDAO.list()); //pass this assume to uodate the menu.
	mv.addObject("category",category);
	mv.addObject("userClickCategory",true);
	return mv; 
}

@RequestMapping(value = "/show/{id}/newsfeed")
public ModelAndView showSingleNews(@PathVariable("id") int id) {
	ModelAndView mv = new ModelAndView("page");
	NewsFeed newsFeed = null;
	newsFeed = newsFeedDAO.get(id);
	//Updating the view count
	//newsFeed.setViews(newsFeed.getViews()+1);
	//newsFeedDAO.update(newsFeed);
	mv.addObject("title",newsFeed.getHeading());
	mv.addObject("categoryDAO",categoryDAO.list());
	mv.addObject("newsFeed",newsFeed);
	mv.addObject("userClickSingleNews",true);
	return mv; 
}

@RequestMapping(value = "/show/{id}/newsondiv")
public ModelAndView showNewsOnDiv(@PathVariable("id") int id) {
	ModelAndView mv = new ModelAndView("page");
	Category category = null;
	category = categoryDAO.get(id);
	mv.addObject("newsFeedDAO",newsFeedDAO.listOfNewsInDiv(id));
	mv.addObject("categoryDAO",categoryDAO.list());
	mv.addObject("userClickAddtoDiv",true);
	mv.addObject("category",category);
	return mv; 
}

//Adding the login page
@RequestMapping(value = "/login")
public ModelAndView login(@RequestParam(name="error",required=false)String error) {
	ModelAndView mv = new ModelAndView("login");
	if(error!=null) {
		mv.addObject("message","Invalid UserName and Password!");
	}
	mv.addObject("title","Login");
	return mv; 
}

@RequestMapping(value = "/access-denied")
public ModelAndView accessDenied() {
	ModelAndView mv = new ModelAndView("error");
	mv.addObject("title","403 - Access Denied");
	mv.addObject("errorTitle","403");
	mv.addObject("errorDescription","NOT AUTHORIZED");
	return mv; 
}


	
	
}
