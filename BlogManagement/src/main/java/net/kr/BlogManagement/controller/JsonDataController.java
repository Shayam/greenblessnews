package net.kr.BlogManagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.kr.BlogManagementBackend.dao.NewsFeedDAO;
import net.kr.BlogManagementBackend.dto.NewsFeed;

@Controller
@RequestMapping("/json/data")
public class JsonDataController {
	
	@Autowired
	private NewsFeedDAO newsfeedDAO;
	@RequestMapping("/all/newsfeed")
	@ResponseBody
	public List<NewsFeed> getAllNewsFeed(){
		
		return newsfeedDAO.listActiveNews();
	} 
	
	@RequestMapping("/category/{id}/newsfeed")
	@ResponseBody
	public List<NewsFeed> getNewsByCategory(@PathVariable int id){
		
		return newsfeedDAO.listActiveNewsByCategory(id);
	} 
}
