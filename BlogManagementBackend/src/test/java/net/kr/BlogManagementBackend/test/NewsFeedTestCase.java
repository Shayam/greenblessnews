//package net.kr.BlogManagementBackend.test;
//
//import static org.junit.Assert.assertEquals;
//
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//
//import net.kr.BlogManagementBackend.dao.NewsFeedDAO;
//import net.kr.BlogManagementBackend.dto.NewsFeed;
//
//public class NewsFeedTestCase {
//	private static AnnotationConfigApplicationContext context;
//	private static NewsFeedDAO newsFeedDAO;
//	private NewsFeed newsFeed;
//
//	@BeforeClass
//	public static void init() {
//		context = new AnnotationConfigApplicationContext();
//		context.scan("net.kr.BlogManagementBackend");
//		context.refresh();
//
//		newsFeedDAO = (NewsFeedDAO) context.getBean("NewsFeedDAO");
//	}
//	
//	@Test
//	public void testNewsFeed() {
//		newsFeed = new NewsFeed();
//		newsFeed.setCategoryId(1);
//		newsFeed.setHeading("FirstHeading");
//		newsFeed.setDescription("Every Where");
//		newsFeed.setImage("test.png");
//		assertEquals("Succesfully added a News inside a table!.",true, newsFeedDAO.add(newsFeed));
//		
//		newsFeed = newsFeedDAO.get(1);
//		assertEquals("Succesfully fetched a single newsFeed from the table!.","Heading 1", newsFeed.getHeading());
//		
//		newsFeed = newsFeedDAO.get(1);
//		newsFeed.setDescription("Updated Description");
//		assertEquals("Succesfully Updated a newsFeed in the table!.",true, newsFeedDAO.update(newsFeed));
//		
//		newsFeed = newsFeedDAO.get(2);
//		assertEquals("Succesfully Deleted a newsFeed in the table!.",true, newsFeedDAO.delete(newsFeed));
//		
//		
//		assertEquals("Succesfully fetch list of newsFeed from the table!.", 5, newsFeedDAO.list().size());
//		
//		assertEquals("Succesfully fetch list of newsFeed from the table!.", 4, newsFeedDAO.listActiveNews().size());
//		
//		assertEquals("Succesfully fetch list of newsFeed from the table!.", 4, newsFeedDAO.listActiveNewsByCategory(1).size());
//		assertEquals("Succesfully fetch list of newsFeed from the table!.", 2, newsFeedDAO.getLatestActiveNews(2).size());
//		
//		
//	}
//}
