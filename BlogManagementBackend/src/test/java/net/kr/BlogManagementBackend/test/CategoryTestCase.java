//package net.kr.BlogManagementBackend.test;
//
//import static org.junit.Assert.assertEquals;
//
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//
//import net.kr.BlogManagementBackend.dao.CategoryDAO;
//import net.kr.BlogManagementBackend.dto.Category;
//
//public class CategoryTestCase {
//	private static AnnotationConfigApplicationContext context;
//	private static CategoryDAO categoryDAO;
//	private Category category;
//
//	@BeforeClass
//	public static void init() {
//		context = new AnnotationConfigApplicationContext();
//		context.scan("net.kr.BlogManagementBackend");
//		context.refresh();
//
//		categoryDAO = (CategoryDAO) context.getBean("CategoryDAO");
//	}
//
////	@Test
////	public void testAddCategory() {
////		category = new Category();
////		category.setName("Vender");
////		category.setDescription("Testing Purpose");
////		category.setImageURL("test.png");
////		assertEquals("Succesfully added a category inside a table!.",true, categoryDAO.add(category));
////	}
//
////	@Test
////	public void testGetCategory() {
////		
////		category = categoryDAO.get(1);
////		assertEquals("Succesfully fetched a single category from the table!.","Vender", category.getName());
////	}
//
////	@Test
////	public void testUpdateCategory() {
////		
////		category = categoryDAO.get(1);
////		category.setName("Vender");
////		assertEquals("Succesfully Updated a category in the table!.",true, categoryDAO.update(category));
////	}
//
////	@Test
////	public void testDeleteCategory() {
////		
////		category = categoryDAO.get(1);
////		assertEquals("Succesfully Deleted a category in the table!.",true, categoryDAO.delete(category));
////	}
//
//	@Test
//	public void testListCategory() {
//
//		assertEquals("Succesfully fetch list of category from the table!.", 1, categoryDAO.list().size());
//		// in hear 1 mean now in my database only 1 row that satisfy. we have to give
//		// how many rows will return
//	}
//	
//	//You cna write above every test case in a single test case. Better is that way. 
//}
