package net.kr.BlogManagementBackend.daoimpl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.kr.BlogManagementBackend.dao.NewsFeedDAO;
import net.kr.BlogManagementBackend.dto.Category;
import net.kr.BlogManagementBackend.dto.NewsFeed;

@Repository("NewsFeedDAO") // in hear will show a error. To fix it we have to add the dependency to POM.The
// name must be name as the controller given name
@Transactional
public class NewsFeedDAOImpl implements NewsFeedDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public NewsFeed get(int newsid) {
		try {
			return sessionFactory.getCurrentSession().get(NewsFeed.class, Integer.valueOf(newsid));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public List<NewsFeed> list() {
		return sessionFactory.getCurrentSession().createQuery("From NewsFeed", NewsFeed.class).getResultList();
	}
	
	
	//Checking to get the news by category
	@Override
	public List<NewsFeed> listOfNewsInDiv(int id) {
		//In hear will get only the active categories
		String selectNewsById = "From NewsFeed WHERE categoryid = :categoryid";
		Query query = sessionFactory.getCurrentSession().createQuery(selectNewsById);
		query.setParameter("categoryid", id);
		return query.getResultList();
	}

	@Override
	public boolean add(NewsFeed news) {
		try {
			sessionFactory.getCurrentSession().persist(news);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean update(NewsFeed news) {
		try {
			sessionFactory.getCurrentSession().update(news);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean delete(NewsFeed news) {
		 // in hear we are not deleting a category from the database. rather than doing
		// that we are just set the active flag to false or 0
		try {
			news.setActive(false);
			return this.update(news);
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public List<NewsFeed> listActiveNews() {
		String selectActiveNews = "FROM NewsFeed WHERE active = :active";
		return sessionFactory.getCurrentSession()
				.createQuery(selectActiveNews,NewsFeed.class)
				.setParameter("active", true)
				.getResultList();
	}

	@Override
	public List<NewsFeed> listActiveNewsByCategory(int categoryId) {
		String selectActiveNewsByCategory = "FROM NewsFeed WHERE active = :active and CategoryId = :categoryId";
		return sessionFactory.getCurrentSession()
				.createQuery(selectActiveNewsByCategory,NewsFeed.class)
				.setParameter("active", true)
				.setParameter("categoryId", categoryId)
				.getResultList();
	}

	@Override
	public List<NewsFeed> getLatestActiveNews(int count) {
		return sessionFactory.getCurrentSession()
				.createQuery("FROM NewsFeed where active= :active ORDER BY id",NewsFeed.class)
				.setParameter("active", true)
				.setFirstResult(0)
				.setMaxResults(count)
				.getResultList();
	}
}
