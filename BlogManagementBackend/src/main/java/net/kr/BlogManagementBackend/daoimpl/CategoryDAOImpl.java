package net.kr.BlogManagementBackend.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.kr.BlogManagementBackend.dao.CategoryDAO;
import net.kr.BlogManagementBackend.dto.Category;

@Repository("CategoryDAO") // in hear will show a error. To fix it we have to add the dependency to POM.The
							// name must be name as the controller given name
@Transactional
public class CategoryDAOImpl implements CategoryDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static List<Category> Categories = new ArrayList<>();

	@Override
	public List<Category> list() {
		//In hear will get only the active categories
		String selectActiveCategory = "From Category WHERE active = :active";
		Query query = sessionFactory.getCurrentSession().createQuery(selectActiveCategory);
		query.setParameter("active", true);
		return query.getResultList();
	}

	// Getting Single Category Based on ID
	@Override
	public Category get(int id) {
		// Enhanced for loop
		return sessionFactory.getCurrentSession().get(Category.class, Integer.valueOf(id));
	}

	@Override

	public boolean add(Category category) {
		try {
			sessionFactory.getCurrentSession().persist(category);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	/*
	 * Updating a single Category
	 */
	@Override
	public boolean update(Category category) {
		try {
			sessionFactory.getCurrentSession().update(category);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean delete(Category category) {
		category.setActive(false); // in hear we are not deleting a category from the database. rather than doing
									// that we are just set the active flag to false or 0

		try {
			sessionFactory.getCurrentSession().update(category);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

}
