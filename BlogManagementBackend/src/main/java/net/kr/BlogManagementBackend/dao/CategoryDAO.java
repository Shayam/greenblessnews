package net.kr.BlogManagementBackend.dao;

import java.util.List;

import net.kr.BlogManagementBackend.dto.Category;

public interface CategoryDAO {
	
	
	List<Category>list();
	Category get(int id);
	//To add update and delete a category we have to add the folowing methods.
	boolean add(Category category);
	boolean update(Category category);
	boolean delete(Category category);
}
