package net.kr.BlogManagementBackend.dao;

import java.util.List;

import net.kr.BlogManagementBackend.dto.NewsFeed;

public interface NewsFeedDAO {
	List<NewsFeed>list();
	List<NewsFeed> listOfNewsInDiv(int id);
	NewsFeed get(int id);
	//To add update and delete a category we have to add the folowing methods.
	boolean add(NewsFeed news);
	boolean update(NewsFeed news);
	boolean delete(NewsFeed news);
	
	//Business Methods
	List<NewsFeed> listActiveNews();
	List<NewsFeed> listActiveNewsByCategory(int categoryId);
	List<NewsFeed> getLatestActiveNews(int count);
}
