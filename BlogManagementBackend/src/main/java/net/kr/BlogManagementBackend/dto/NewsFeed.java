package net.kr.BlogManagementBackend.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class NewsFeed {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private int idNewsFeed;
	
	
	private int CategoryId;
	
	private String Heading;
	private String Description;
	private String Image;
	@Column(name ="IS_ACTIVE")
	@JsonIgnore
	private boolean active = true;
	@JsonIgnore
	private int Priority;

	
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public int getIdNewsFeed() {
		return idNewsFeed;
	}
	public void setIdNewsFeed(int idNewsFeed) {
		this.idNewsFeed = idNewsFeed;
	}
	public int getCategoryId() {
		return CategoryId;
	}
	public void setCategoryId(int categoryId) {
		CategoryId = categoryId;
	}
	public String getHeading() {
		return Heading;
	}
	public void setHeading(String heading) {
		Heading = heading;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getImage() {
		return Image;
	}
	public void setImage(String image) {
		Image = image;
	}
	public int getPriority() {
		return Priority;
	}
	public void setPriority(int priority) {
		Priority = priority;
	}
	
	
}
